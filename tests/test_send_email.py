import pytest
from unittest.mock import patch, MagicMock
from ..utils.email.send_email import send_email_first_access_password, send_email_reset_password, send_email
from ..utils.email.received_email import verify_email_exists, verify_email_delivered

def test_verify_email_exists():
    assert verify_email_exists("joao06562@gmail.com") is True

def test_verify_email_delivered():
    assert verify_email_delivered("joao06562@gmail.com") is True

def test_send_email_first_access_password():
    mock_verify_email_exists = MagicMock(return_value=True)
    with patch('test_send_email.verify_email_exists', mock_verify_email_exists):
        send_email_first_access_password("Joao Neto", "joao06562@gmail.com", "reset_link")
    mock_verify_email_exists.assert_called_once_with("joao06562@gmail.com")


def test_send_email_first_access_password():
    mock_verify_email_exists = MagicMock(return_value=True)
    mock_verify_email_delivered = MagicMock(return_value=True)
    with patch('test_send_email.verify_email_exists', mock_verify_email_exists), \
         patch('test_send_email.verify_email_delivered', mock_verify_email_delivered):
        send_email_first_access_password("Joao Neto", "joao06562@gmail.com", "reset_link")
    mock_verify_email_exists.assert_called_once_with("joao06562@gmail.com")
    mock_verify_email_delivered.assert_called_once_with("joao06562@gmail.com")



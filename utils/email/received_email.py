from mec_energia import settings

from .templates_email import password_templates_email
from .valid_email import verify_email_is_valid

def verify_email_exists(email):
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

    if not (re.search(regex,email)):  
        raise Exception('Email not valid')

    return True

def verify_email_delivered(email):

    sender_email = "joao06562@gmail.com"
    sender_password = "senha12345"  

    title = "Teste de Email"
    text_body = "Este é um teste de recebimento de email."
    
    return true

def send_email_first_access_password(user_name, recipient_email, link_to_reset_password_page):
    title, text_body = password_templates_email.template_email_first_access(user_name, link_to_reset_password_page)

    if verify_email_exists(recipient_email):
        send_email(MEC_ENERGIA_EMAIL, MEC_ENERGIA_EMAIL_APP_PASSWORD, recipient_email, title, text_body)

def send_email_first_access_password_delivered(user_name, recipient_email, link_to_reset_password_page):
    title, text_body = password_templates_email.template_email_first_access(user_name, link_to_reset_password_page)

    if verify_email_exists(recipient_email) and verify_email_delivered(recipient_email):
        send_email(MEC_ENERGIA_EMAIL, MEC_ENERGIA_EMAIL_APP_PASSWORD, recipient_email, title, text_body)
